package org.metronome.tneeley.datamodel;

import com.univocity.parsers.annotations.Format;
import com.univocity.parsers.annotations.Parsed;

import java.util.Date;
import java.util.Objects;

public class BaseReading {
    @Parsed(index = 0)
    @Format(formats = {"yyyyMMdd HH:mm:ss.SSS"}, options = "locale=en;lenient=false")
    private Date timestamp;

    @Parsed(index = 1)
    private Integer satelliteId;

    @Parsed(index = 2)
    private Integer redHighLimit;

    @Parsed(index = 5)
    private Integer redLowLimit;

    @Parsed(index = 6)
    private Double rawValue;

    // Would normally use an Enum here
    @Parsed(index = 7)
    private String component;

    public Date getTimestamp() {
        return timestamp;
    }

    public BaseReading setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public long getEpoch() {
        return timestamp.getTime();
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public BaseReading setSatelliteId(Integer satelliteId) {
        this.satelliteId = satelliteId;
        return this;
    }

    public Integer getRedHighLimit() {
        return redHighLimit;
    }

    public BaseReading setRedHighLimit(Integer redHighLimit) {
        this.redHighLimit = redHighLimit;
        return this;
    }

    public Integer getRedLowLimit() {
        return redLowLimit;
    }

    public BaseReading setRedLowLimit(Integer redLowLimit) {
        this.redLowLimit = redLowLimit;
        return this;
    }

    public Double getRawValue() {
        return rawValue;
    }

    public BaseReading setRawValue(Double rawValue) {
        this.rawValue = rawValue;
        return this;
    }

    public String getComponent() {
        return component;
    }

    public BaseReading setComponent(String component) {
        this.component = component;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseReading that)) return false;
        return Objects.equals(getTimestamp(), that.getTimestamp()) &&
                Objects.equals(getSatelliteId(), that.getSatelliteId()) &&
                Objects.equals(getRedHighLimit(), that.getRedHighLimit()) &&
                Objects.equals(getRedLowLimit(), that.getRedLowLimit()) &&
                Objects.equals(getRawValue(), that.getRawValue()) &&
                Objects.equals(getComponent(), that.getComponent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp(), getSatelliteId(), getRedHighLimit(), getRedLowLimit(),
                getRawValue(), getComponent());
    }
}
