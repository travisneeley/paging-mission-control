package org.metronome.tneeley.datamodel;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

public class Alert {

    // Understanding that this old mechanism is not advisable since Java 8
    // Time zones are annoying...
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private final Integer satelliteId;
    private final String severity;
    private final String component;
    private final String timestamp;

    public Alert(BaseReading reading) {
        this.satelliteId = reading.getSatelliteId();
        this.component = reading.getComponent();
        this.timestamp = DATE_FORMAT.format(Date.from(Instant.ofEpochMilli(reading.getEpoch())));

        if (reading.getRawValue() < reading.getRedLowLimit()) {
            this.severity = "RED LOW";
        } else if (reading.getRedHighLimit() < reading.getRawValue()) {
            this.severity = "RED HIGH";
        } else {
            this.severity = "UNK";
        }
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public String getComponent() {
        return component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Alert alert)) return false;
        return Objects.equals(getSatelliteId(), alert.getSatelliteId()) &&
                Objects.equals(getSeverity(), alert.getSeverity()) &&
                Objects.equals(getComponent(), alert.getComponent()) &&
                Objects.equals(getTimestamp(), alert.getTimestamp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSatelliteId(), getSeverity(), getComponent(), getTimestamp());
    }
}