package org.metronome.tneeley;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvFormat;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.metronome.tneeley.datamodel.Alert;
import org.metronome.tneeley.datamodel.BaseReading;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class App {
    private static final long FIVE_MINUTES = 300000;
    private static final Charset CHARSET = StandardCharsets.US_ASCII;
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws Exception {
        if (1 != args.length)
            throw new IllegalArgumentException("Exactly one argument must be passed");

        // Get a list of Java beans using Univocity annotated class
        // Throw runtime exception if this fails
        List<BaseReading> baseReadings = extractReadings(args[0]);

        // Group the readings and create alerts
        List<Alert> alerts = generateAlerts(baseReadings);

        // Log those alerts to System.out
        System.out.println(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(alerts));
    }

    public static List<BaseReading> extractReadings(String fileName) {
        try (Reader inputReader = new InputStreamReader(new FileInputStream(fileName), CHARSET)) {
            BeanListProcessor<BaseReading> rowProcessor = new BeanListProcessor<>(BaseReading.class);

            CsvFormat format = new CsvFormat();
            format.setDelimiter("|");
            // Should I trust the system's line separator?

            CsvParserSettings settings = new CsvParserSettings();
            settings.setHeaderExtractionEnabled(false);
            settings.setFormat(format);
            settings.setProcessor(rowProcessor);

            CsvParser parser = new CsvParser(settings);
            parser.parse(inputReader);
            return rowProcessor.getBeans();
        } catch (Exception ex) {
            System.err.println("Exception thrown while reading input file " + fileName);
            System.err.println("Thrown Exception: " + ex.getMessage());
            throw new IllegalStateException("No BaseReading Objects obtained from File");
        }
    }

    public static List<Alert> generateAlerts(List<BaseReading> readings) {
        List<Alert> alerts = new ArrayList<>();

        Multimap<String, BaseReading> alertReadings = ArrayListMultimap.create();

        // Group by satellite, component, and alert type
        // Guava library used to avoid handling creating of list behind the Multimap
        Integer satelliteId;
        Integer redHighLimit;
        Integer redLowLimit;
        Double rawValue;
        String component;
        for (BaseReading reading : readings) {
            satelliteId = reading.getSatelliteId();
            redHighLimit = reading.getRedHighLimit();
            redLowLimit = reading.getRedLowLimit();
            rawValue = reading.getRawValue();
            component = reading.getComponent();

            if (rawValue < redLowLimit) {
                alertReadings.put(satelliteId + "-" + component + "-RED_LOW", reading);
            } else if (rawValue > redHighLimit) {
                alertReadings.put(satelliteId + "-" + component + "-RED_HIGH", reading);
            }
        }

        // For each group
        for (Map.Entry<String, Collection<BaseReading>> entry : alertReadings.asMap().entrySet()) {
            Collection<BaseReading> groupedReadings = entry.getValue();

            // If group size is less than three no alert could be generated regardless, skip
            if (groupedReadings.size() < 3)
                continue;

            // Find the lowest Epoch value since the wording of the problem suggests that it's not every
            // 5 minutes compared to epoch but every five minutes compared to the earliest timestamp of that
            // alert type
            long lowestEpoch = groupedReadings.stream()
                    .min(Comparator.comparingLong(BaseReading::getEpoch)).get().getEpoch();

            // Bucket the alerts by offsetting by the lowest epoch and binning by five minute intervals
            Map<Long, List<BaseReading>> grouped = groupedReadings.stream()
                    .collect(Collectors.groupingBy(reading -> (reading.getEpoch() - lowestEpoch) / FIVE_MINUTES));

            // Filter where in a bucket there are more than three alert readings and generate an Alert object
            // Make sure to use the lowest timestamp of that group
            grouped.values().stream()
                    .filter(group -> group.size() >= 3)
                    .forEach(group -> alerts.add(group.stream()
                            .min(Comparator.comparingLong(BaseReading::getEpoch))
                            .map(Alert::new)
                            .get()
                    ));
        }

        return alerts;
    }
}
