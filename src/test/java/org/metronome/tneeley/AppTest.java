package org.metronome.tneeley;

import org.junit.Assert;
import org.junit.Test;
import org.metronome.tneeley.datamodel.Alert;
import org.metronome.tneeley.datamodel.BaseReading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class AppTest {
    // Understanding that this old mechanism is not advisable since Java 8
    // Time zones are annoying...
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    @Test
    public void testFileExtractionValidity() throws ParseException {
        List<BaseReading> readings = App.extractReadings("src/test/resources/singleReading.csv");
        Assert.assertEquals("Size mismatch", 1, readings.size());

        BaseReading expectedReading = new BaseReading()
                .setTimestamp(DATE_FORMAT.parse("20180101 23:01:05.001"))
                .setSatelliteId(1001)
                .setRedHighLimit(101)
                .setRedLowLimit(20)
                .setRawValue(99.9)
                .setComponent("TSTAT");

        BaseReading baseReading = readings.get(0);
        Assert.assertEquals(expectedReading, baseReading);
    }

    @Test
    public void testFileExtractionCount() {
        List<BaseReading> readings = App.extractReadings("src/test/resources/multipleReadings.csv");
        Assert.assertEquals("Size mismatch", 14, readings.size());
    }

    @Test
    public void testAlertGeneration() {
        List<Alert> alerts = App.generateAlerts(App.extractReadings("src/test/resources/multipleReadings.csv"));
        Assert.assertEquals("Size mismatch", 2, alerts.size());
    }
}
